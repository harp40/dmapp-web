import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    REGISTER_REQUEST,
    REGISTER_SUCCESS,
    REGISTER_FAILURE,
    LOGOUT,
    SEARCH,
    GET_CONVERSATIONS,
    GET_ALL_MESSAGES,
    GET_MESSAGES,
} from '../actions';

const initialState = {
    data: [],
    messages: [{conversationId: ''}],
    filteredData: []
    };

export function messagesReducer(state = initialState, action) {
    switch (action.type) {
        case GET_MESSAGES:
            const {messages} = action;
            return {
                ...state,
                messages,
            };
        default:
            return state;
    }
}

export function conversationsReducer(state =initialState, action) {
    switch(action.type) {
        case GET_CONVERSATIONS:
            const {data} = action;
            return {
                ...state,
                data,
                filteredData: data
            };
        case GET_ALL_MESSAGES:
            const {messages} = action;
            return {
                ...state,
                messages
            };
        case SEARCH:
            const filteredData = action.displayedConversations;
            return {
                ...state,
                filteredData
            };
        default:
            return state
    }
 }

export function authReducer(state = {}, action) {
    switch (action.type) {
        case LOGIN_REQUEST:
            return {
                isUserLogged: true,
                user: action.user,
            };
        case LOGIN_SUCCESS:
            return {...state,
                isUserLogged: true,
                user: action.user,
                email: action.email,
                answer: 'Авторизация успешна!',
                status: 'success',
            };
        case LOGIN_FAILURE:
            return {
                answer: 'Неправильный email или пароль!',
                status: 'danger',
            };
        case LOGOUT:
            return {
                user:'',
               //answer:'Вы успешно вышли',
               // status: 'success',
                isUserLogged: false,
            };
        default:
            return state;
    }
}

export function registerReducer(state = {}, action) {
    switch (action.type) {
        case REGISTER_REQUEST:
            return {
                isUserLogged: true,
                user: action.user,
            };
        case REGISTER_SUCCESS:
            return {
                isUserLogged: true,
                answer: 'регистрация успешна',
                status: 'success',
            };
        case REGISTER_FAILURE:
            return {
                answer: 'регистрация не удалась',
                status: 'danger',
            };
        default:
            return state;
    }
}
