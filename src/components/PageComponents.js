import React from "react";
import {Button} from "reactstrap";
import '../HomePage.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faComment,
    faCommentSlash,
    faSave
} from '@fortawesome/free-solid-svg-icons';
import {Link, Route} from "react-router-dom";
import {Input} from 'reactstrap'

import {ConversationsComponents} from './ConversationsComponents'
import {MessagesComponents} from './MessagesComponents'


const SideBar =() => {
    return (
        <div className='sidebar'>
            <div className='dialog-card dialogs-active'>
                <Link to='/active' className='dialog-link'>
                    <FontAwesomeIcon
                        className="fa-icons"
                        icon={faComment}
                    />
                    <span>Активные</span>
                </Link>
            </div>

            <div className='dialog-card dialogs-done'>
                <Link to='/closed' className='dialog-link'>
                    <FontAwesomeIcon
                        className="fa-icons"
                        icon={faCommentSlash}
                    />
                    <span>Завершенные</span>
                </Link>
            </div>

            <div className='dialog-card dialogs-saved'>
                <Link to='/saved' className='dialog-link'>
                    <FontAwesomeIcon
                        className="fa-icons"
                        icon={faSave}
                    />
                    <span>Сохраненные</span>
                </Link>
            </div>
        </div>
    )
};

const Header = (props) => {
    return (
        <div className='header'>
            <div className='header-container'>
                <div className='header-up'>
                    <Link to="/"><h1>{props.email}</h1></Link>
                    {props.user?
                        <Button onClick={()=>props.LogOut()}>Выйти</Button>
                        :null}
                </div>
                <Search handleSearchChange={props.handleSearchChange}/>
            </div>


        </div>
    )
};

const Body = (props) => {
    const {Dialogs} = ConversationsComponents;
    const {Messages} = MessagesComponents;
    const {dialogs, saveDialog, messages, beginDialog, deleteDialog, messagesArray, id} = props;
    const savedDialogs = dialogs.filter(el => el.status.includes("SAVED"));
    const activeDialogs = dialogs.filter(el => el.status.includes("ACTIVE"));
    const closedDialogs = dialogs.filter(el => el.status.includes("CLOSED"));
    const pendingDialogs = dialogs.filter(el => el.status.includes("PENDING"));

    return (
        <div className='main-body'>
            <Route path="/saved">
                <Dialogs messages={messages} dialogs={savedDialogs} deleteDialog={deleteDialog}/>
            </Route>
            <Route path="/active">
                <Dialogs messages={messages} dialogs={activeDialogs} saveDialog={saveDialog} beginDialog={beginDialog}/>
            </Route>
            <Route path="/closed">
                <Dialogs messages={messages} dialogs={closedDialogs} saveDialog={saveDialog}/>
            </Route>

            <Route path={`/conversation/${id}`}>
                <Messages id={id} messages={messagesArray}/>
            </Route>

             <Route exact path="/">
                <Dialogs messages={messages} dialogs={pendingDialogs} beginDialog={beginDialog}/>
            </Route>
        </div>
    )
};

const Search = (props) => {
    const {handleSearchChange} = props;
    return (
        <div className='search'>
            <Input
                type='search'
                name='search'
                placeholder='Поиск'
                onChange={(e)=>handleSearchChange(e)}
            />
        </div>

    )
};

export const PageComponents = {
    SideBar,
    Header,
    Body,
    Search,
};