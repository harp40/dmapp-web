import React from "react";
import {
    ListGroup,
    ListGroupItem,
} from 'reactstrap'
import moment from 'moment'
import 'moment/locale/ru';
import './MessagesComponents.css'


const Messages = props => {
    const {messages} = props;
    const message = messages.map((messages, index) => {
        return (
            <ListGroupItem key={index}>
                <Message
                    messages={messages}
                />
            </ListGroupItem>
        )
    });
    return (
        <ListGroup>
            {message}
        </ListGroup>
    )
};

const Message = props => {
    const {authorId, body, createdAt} = props.messages;
    const time = moment(createdAt).calendar();
    return (
        <div className="message-item">
            <div className="author">{authorId}</div>
            <div className="message-body">{body}</div>
            <div className="time">{time}</div>
        </div>
    )
};

export const MessagesComponents = {Messages};