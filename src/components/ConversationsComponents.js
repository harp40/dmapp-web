import React from "react";
import {
    Button,
    ListGroup,
    ListGroupItem,
} from 'reactstrap'
import './CinversationsComponents.css'
import moment from 'moment'
import 'moment/locale/ru';
import {Link} from 'react-router-dom'

const DialogCard = props => {
    const {id, authorId, updatedAt, status} = props.dialogs;
    const {messages} = props;
    const query = messages.filter(el =>
         el.conversationId.includes(id));
    const time = moment(updatedAt).fromNow();
    return (
        <div className="dialog-card">
            <div className="client-card-area">
                <div className="photo">
                    <img alt="someone" src="https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"/>
                </div>
                <span>{authorId}</span>
            </div>

            <div className='text-area'>
                <p>{query[0]?query[0].body:null}
                </p>
            </div>

            <div className='functional-area'>
                {status==='ACTIVE'?
                    <Link to={`/conversation/${id}`} >
                        <Button onClick={() => props.beginDialog(props.dialogs.id)}>
                            Продолжить
                        </Button>
                    </Link>
                    :null}
                <span>Последнее обновление {time}</span>
                {status==='SAVED'||status==='CLOSED'?<div className='rating-block'>
                    <span>Оценка</span>
                    <div>компонент с оценкой</div>
                </div>:null}
                {props.deleteDialog?<Button onClick={() => props.deleteDialog(props.dialogs.id)}>Удалить</Button>:null}
                {props.saveDialog?<Button onClick={() => props.saveDialog(props.dialogs.id)}>Сохранить</Button>:null}
                {status==='PENDING'?<Button onClick={() => props.beginDialog(props.dialogs.id)}>Начать</Button>:null}
            </div>
        </div>
    )
};

const Dialogs = props => {
    const {messages} = props;
    const {beginDialog, saveDialog, deleteDialog} = props;
    const rows = props.dialogs.map((dialogs, index) =>{
        return (
            <ListGroupItem key={index}>
                <DialogCard
                    messages={messages}
                    beginDialog={beginDialog}
                    deleteDialog={deleteDialog}
                    dialogs={dialogs}
                    saveDialog={saveDialog}/>
            </ListGroupItem>
        )
    });
    return (
        <ListGroup>
            {rows}
        </ListGroup>
    )
};

export const ConversationsComponents = {Dialogs,DialogCard};