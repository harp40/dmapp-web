import React from 'react';
import { Control, Errors } from 'react-redux-form';
import {
    Button,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Input,
} from 'reactstrap';
import '../style.css';

const required = value => value && value.length;

const Password = props => {
    const { handleChange, id, label } = props;
    return (
        <InputGroup className="input-container">
            <InputGroupAddon color="info" addonType="prepend">
                <InputGroupText>{label}</InputGroupText>
            </InputGroupAddon>
            <Control
                type="password"
                model={id}
                id={id}
                component={Input}
                validators={{
                    required,
                }}
                onChange={handleChange}
            />
            <Errors
                className="errors"
                model={id}
                show="touched"
                messages={{
                    required: '*Пароль не может быть пустым!',
                }}
            />
        </InputGroup>
    );
};

const RegPassword = props => {
    const { handleChange, id, label } = props;
    return (
        <InputGroup className="input-container">
            <InputGroupAddon addonType="prepend">
                <InputGroupText>{label}</InputGroupText>
            </InputGroupAddon>
            <Control
                type="password"
                model={id}
                id={id}
                component={Input}
                validators={{
                    required,
                    validPassword: value =>
                        /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/.test(
                            value
                        ),
                }}
                onChange={handleChange}
            />
            <Errors
                className="errors"
                model={id}
                show="touched"
                title="Пароль должен содержать цифру, буквы в нижнем и верхнем регистре и иметь длину не менее 8 знаков!"
                messages={{
                    required: '*Пароль не может быть пустым!',
                    validPassword: '*Слишком простой пароль!',
                }}
            />
        </InputGroup>
    );
};

const ConfirmPassword = props => {
    const { handleChange, id, label, pass } = props;
    const confirm = value => {
        return value === pass.password;
    };
    return (
        <InputGroup className="input-container">
            <InputGroupAddon addonType="prepend">
                <InputGroupText>{label}</InputGroupText>
            </InputGroupAddon>
            <Control
                type="password"
                model={id}
                id={id}
                component={Input}
                validators={{
                    required,
                    confirm,
                }}
                onChange={handleChange}
            />
            <Errors
                className="errors"
                model={id}
                show="touched"
                messages={{
                    required: '*Пароль не может быть пустым!',
                    confirm: '*Пароли Должны совпадать!',
                }}
            />
        </InputGroup>
    );
};

const Email = props => {
    const { handleChange, id, label } = props;
    return (
        <InputGroup className="input-container">
            <InputGroupAddon addonType="prepend">
                <InputGroupText>{label}</InputGroupText>
            </InputGroupAddon>
            <Control
                type="email"
                model=".email"
                id={id}
                component={Input}
                validators={{
                    required,
                    validEmail: value =>
                        /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value),
                }}
                onChange={handleChange}
            />
            <Errors
                className="errors"
                model=".email"
                show="touched"
                messages={{
                    required: '*E-mail не может быть пустым!',
                    validEmail: '*Неправильный e-mail адрес',
                }}
            />
        </InputGroup>
    );
};

const FormButton = props => {
    return <Button type="submit">{props.value}</Button>;
};

export const FormComponents = {
    Password,
    Email,
    FormButton,
    ConfirmPassword,
    RegPassword,
};
