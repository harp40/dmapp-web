import { userService } from './services/service';
import { toast } from 'react-toastify';
import {unitedSearch} from './helpers/search'

export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const REGISTER_REQUEST = 'REGISTER_REQUEST';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILURE = 'REGISTER_FAILURE';
export const LOGOUT = 'LOGOUT';
export  const SEARCH = 'SEARCH';
export const SAVE_DIALOG = 'SAVE_DIALOG';
export const GET_CONVERSATIONS = 'GET_CONVERSATIONS';
export const GET_ALL_MESSAGES = 'GET_ALL_MESSAGES';
export const GET_MESSAGES = 'GET_MESSAGES';

export function save () {
    return {
        type: SAVE_DIALOG,       
    }
}

export function getMessages(id) {
    return dispatch => {
         userService.getMessages(id)
             .then(messages => {
                 dispatch(success(messages))
             })
    };
    function success(messages) {
        return {type: GET_MESSAGES, messages};
    }
}

export function search (data,messages, conversations) {
    const displayedConversations = unitedSearch(data,messages, conversations);
    return {
        type: SEARCH,
        displayedConversations,
    }
}

export function getConversations () {
    return dispatch => {
        userService.getConversations()
            .then(
                data => {
                    dispatch(success(data));
                    dispatch(getAllMessages(data));
                });
            };
    function success(data) {
        return {type: GET_CONVERSATIONS, data};
    }

}

export function getAllMessages (data) {
    return dispatch => {
        let messages = [];
        data.forEach(data => {
             userService.getMessages(data.id)
                 .then(
                     messageList => {
                         messages = messages.concat(messageList);
                         dispatch(success(messages));
                     });
        });
    };
    function success(messages) {
        return {type: GET_ALL_MESSAGES, messages};
    }
}
export function login(user) {
    return dispatch => {
        dispatch(request(user));

        userService.login(user).then(
            response => {
                dispatch(success(response,user.email));
                toast.success('Авторизация успешна!');
                this.history.push('/');
            },
            error => {
                dispatch(failure(error));
            }
        );
    };
    function request(user) {
        return { type: LOGIN_REQUEST, user };
    }
    function success(user, email) {
        return { type: LOGIN_SUCCESS, user ,email};
    }
    function failure(error) {
        return { type: LOGIN_FAILURE, error };
    }
}

export function logout() {
    userService.logout();
    return {
        type: LOGOUT,
    }

}

export function getProfile() {
    return dispatch => {
        const {user} = localStorage;
        if(user) {
            userService.getProfile(user).then(response => {
                if (!response.ok) {
                    if (response.status === 401) {
                        toast.error('Токен недействителен!');
                        dispatch(logout());
                        this.history.push('/login')
                    }
                }
            });
        }}}


export function register(user) {
    return dispatch => {
        dispatch(request(user));

        userService.register(user).then(
            response => {
                dispatch(success(response));
                toast.success('Вы успешно зарегестрировались!');
                this.history.push('/');
            },
            error => {
                dispatch(failure(error));
            }
        );
    };
    function request(user) {
        return { type: REGISTER_REQUEST, user };
    }
    function success(user) {
        return { type: REGISTER_SUCCESS, user };
    }
    function failure(error) {
        return { type: REGISTER_FAILURE, error };
    }
}
