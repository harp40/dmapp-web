import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Provider } from 'react-redux';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import store, {persistor} from './store.js';
import {PersistGate} from "redux-persist/integration/react";
import { LoginForm } from './LoginForm.js';
import { RegisterForm } from './RegisterForm';
import {HomePage} from './HomePage';
import PrivateRoute from "./PrivateRoute";
import { ForgotPassword } from './ForgotPassword';
import UpdatePassword from './UpdatePassword';

import 'bootstrap/dist/css/bootstrap.css';

class App extends React.Component {

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <p>
                        Edit <code>src/App.js</code> and save to reload.
                    </p>
                    <a
                        className="App-link"
                        href="https://reactjs.org"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Learn React
                    </a>
                </header>
                <Provider store={store}>
                    <PersistGate loading={null} persistor={persistor}>
                        <Router history={this.context.history}>
                            <Switch>
                                <Route path="/login">
                                    <LoginForm />
                                </Route>
                                <Route path="/register">
                                    <RegisterForm />
                                </Route>
                                <Route path="/forgot-password">
                                    <ForgotPassword />
                                </Route>
                                <Route path="/update-password">
                                    <UpdatePassword />
                                </Route>
                                <PrivateRoute path="/">
                                    <HomePage />
                                </PrivateRoute>
                            </Switch>
                        </Router>
                        <ToastContainer position="bottom-right" />
                    </PersistGate>

                </Provider>
            </div>
        );
    }
}


export default App
