import React from 'react';
import { Form } from 'react-redux-form';
import { login } from './actions.js';
import { connect } from 'react-redux';
import { FormComponents } from './components/FormComponents';
import { Link, withRouter } from 'react-router-dom';
import { Form as FormComponent, Alert } from 'reactstrap';
import {Redirect} from 'react-router-dom'

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            submitted: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = e => {
        const { name, value } = e.target;
        const newName = name.slice(name.indexOf('.') + 1);
        this.setState({ [newName]: value });
    };

    handleSubmit() {
        this.setState({ submitted: true });
        const { email, password } = this.state;
        if (email && password) {
            const data = { email, password };
            this.props.login(data);
        }
    }

    handleSubmitFailed() {
        //console.log(this.props.answer);
    }

    render() {
        const { Password, Email, FormButton } = FormComponents;
        if(localStorage.user)return <Redirect to='/'/>;
        else return (
            <Form
                className="MyForm"
                model="LoginUser"
                component={FormComponent}
                onSubmit={user => this.handleSubmit(user)}
                onSubmitFailed={this.handleSubmitFailed()}
            >
                <h1 className="form_label">Вход</h1>

                {this.props.answer && (
                    <Alert color={this.props.status} className="login_error">
                        {this.props.answer}
                    </Alert>
                )}

                <Email
                    label="Email"
                    id="LoginUser.email"
                    handleChange={this.handleChange}
                />
                <Password
                    label="Пароль"
                    id="LoginUser.password"
                    handleChange={this.handleChange}
                />

                <div className="form-buttons login-button">
                    <FormButton value="Вход" />
                </div>
                <div className="form-buttons">
                    <Link to="/register" className="link-button">
                        Регистрация
                    </Link>

                    <Link to="/forgot-password" className="link-button">
                        Забыли пароль?
                    </Link>
                </div>
            </Form>
        );
    }
}

function mapState(state) {
    const { isUserLogged, answer, status } = state.authReducer;
    return { isUserLogged, answer, status };
}

const actionCreators = {
    login: login,
};

const connectedLoginPage = withRouter(
    connect(mapState, actionCreators)(LoginForm)
);

export { connectedLoginPage as LoginForm };
