import { toast } from 'react-toastify';

export const userService = {
    login,
    register,
    logout,
    getProfile,
    getConversations,
    getMessages,
    changeConversationStatus,
    deleteConversation,
};

function deleteConversation(id) {
    const {user} = localStorage;
    const token = JSON.parse(user);
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
            'id': `${id}`,
        }
    };
    return fetch(`http://142.93.103.89/conversations/${id}`, requestOptions)
        .then(handleResponse)
}

function getConversations() {
    const {user} = localStorage;
    const token = JSON.parse(user);
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
     };
    return fetch(`http://142.93.103.89/conversations`, requestOptions)
        .then(handleResponse)
}

function getMessages(id) {
    const {user} = localStorage;
    const token = JSON.parse(user);
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
            'id': `${id}`
        },
    };
    return fetch(`http://142.93.103.89/conversations/${id}/messages`, requestOptions)
        .then(handleResponse)
}
function changeConversationStatus(id,status) {
    const {user} = localStorage;
    const token = JSON.parse(user);
    const requestOptions = {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
            'id': `${id}`,
        },
        body: JSON.stringify({
            'status': `${status}`
        })
    };
    return fetch(`http://142.93.103.89/conversations/${id}`, requestOptions)
        .then(handleResponse)
}
/*
function newConversation() {
    const {user} = localStorage;
    console.log(user);
    const token = JSON.parse(user);
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    };
    return fetch(`http://142.93.103.89/conversations`, requestOptions)
        .then(handleResponse)
}
*/
function login(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user),
    };

    return fetch(`http://142.93.103.89/login`, requestOptions)
        .then(handleResponse)
        .then(user => {
            const {access_token} = user;
            //console.log(user)// store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('user', JSON.stringify(access_token));
           // console.log(localStorage.user);
            return access_token;
        });
}

function logout() {
    // remove user from local storage to log user out
   localStorage.removeItem('user');
}

function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user),
    };

    return fetch(`http://142.93.103.89/register`, requestOptions)
        .then(handleResponse);
}



function getProfile(user) {
    const token = JSON.parse(user);
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    };
    return fetch(`http://142.93.103.89/profile`, requestOptions)
}


function handleResponse(response) {
    return response.text().then(text => {

        const data = text && JSON.parse(text);
       // console.log(data);
        if (!response.ok) {
            if (response.status === 401) {
                toast.error('Неправильный email или пароль!');
                // auto logout if 401 response returned from api
               // logout();
            }
            if (response.status === 409) {
                toast.error('Такой пользователь уже существует');
            }
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        return data;
    });
}
