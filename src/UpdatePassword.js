import React from 'react';
import { Form } from 'react-redux-form';
import { FormComponents } from './components/FormComponents';
import { Link } from 'react-router-dom';

class UpdatePassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = e => {
        const { name, value } = e.target;
        const newName = name.slice(name.indexOf('.') + 1);
        this.setState({ [newName]: value });
    };

    handleSubmit() {
        this.setState({ submitted: true });
    }

    handleSubmitFailed() {
        console.log(this.props.answer);
    }

    render() {
        const { RegPassword, ConfirmPassword, FormButton } = FormComponents;
        return (
            <Form
                className="MyForm"
                model="RegisterUser"
                onSubmit={user => this.handleSubmit(user)}
                onSubmitFailed={this.handleSubmitFailed()}
            >
                <h1 className="form_label">Новый пароль</h1>

                <RegPassword
                    label="Пароль"
                    id="RegisterUser.password"
                    handleChange={this.handleChange}
                />

                <ConfirmPassword
                    pass={this.state}
                    label="Повторите пароль"
                    id="RegisterUser.confirmPassword"
                    handleChange={this.handleChange}
                />

                <div className="form-buttons forgot-button">
                    <FormButton value="Подтвердить изменения" />
                </div>
                <div className="form-buttons">
                    <Link to="/login" className="link-button">
                        Войти
                    </Link>
                    <Link to="/register" className="link-button">
                        Регистрация
                    </Link>
                </div>
            </Form>
        );
    }
}

export default UpdatePassword;
