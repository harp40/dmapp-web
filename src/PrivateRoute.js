import React from "react";
import {Route, Redirect} from 'react-router-dom'
const PrivateRoute = ({children, ...rest}) =>(
    <Route {...rest} render={()=> (
        localStorage.user
        ?children
        :<Redirect to='/login'/>
    )}/>
);


export default PrivateRoute