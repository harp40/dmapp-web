import React from 'react';
import { Form } from 'react-redux-form';
import { register } from './actions';
import { connect } from 'react-redux';
import { FormComponents } from './components/FormComponents';
import {Link, Redirect, withRouter} from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGoogle, faVk } from '@fortawesome/free-brands-svg-icons';
import { Form as FormComponent, Alert } from 'reactstrap';

class RegisterForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            confirmPassword: '',
            submitted: false,
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = e => {
        const { name, value } = e.target;
        const newName = name.slice(name.indexOf('.') + 1);
        this.setState({ [newName]: value });
    };

    handleSubmit() {
        this.setState({ submitted: true });
        const { email, password, confirmPassword } = this.state;
        if (email && password && confirmPassword) {
            const data = { email, password };

            this.props.register(data);
        }
    }

    handleSubmitFailed() {
    }

    render() {
        const {
            RegPassword,
            Email,
            FormButton,
            ConfirmPassword,
        } = FormComponents;
        if(localStorage.user) return <Redirect to='/'/>;
        else return (
            <Form
                className="MyForm"
                model="RegisterUser"
                component={FormComponent}
                onSubmit={user => this.handleSubmit(user)}
                onSubmitFailed={this.handleSubmitFailed()}
            >
                <h1 className="form_label">Регистрация</h1>

                {this.props.answer && (
                    <Alert color={this.props.status} className="login_error">
                        {this.props.answer}
                    </Alert>
                )}

                <Email
                    label="Email"
                    id="RegisterUser.email"
                    handleChange={this.handleChange}
                />

                <RegPassword
                    label="Пароль"
                    id="RegisterUser.password"
                    handleChange={this.handleChange}
                />

                <ConfirmPassword
                    pass={this.state}
                    label="Повторите пароль"
                    id="RegisterUser.confirmPassword"
                    handleChange={this.handleChange}
                />

                <div className="form-buttons">
                    <FormButton value="Регистрация" />
                    <div className="social-links">
                        <a href="/register">
                            <FontAwesomeIcon
                                className="fa-icons"
                                icon={faGoogle}
                            />
                        </a>
                        <a href="/register">
                            <FontAwesomeIcon className="fa-icons" icon={faVk} />
                        </a>
                    </div>
                </div>
                <div className="form-buttons">
                    <Link to="/login" className="link-button">
                        Войти
                    </Link>
                    <Link to="/forgot-password" className="link-button">
                        Забыли пароль?
                    </Link>
                </div>
            </Form>
        );
    }
}

function mapState(state) {
    const { answer, status } = state.registerReducer;
    return { answer, status };
}

const actionCreators = {
    register: register,
};

const connectedRegisterPage = withRouter(
    connect(mapState, actionCreators)(RegisterForm)
);

export { connectedRegisterPage as RegisterForm };
