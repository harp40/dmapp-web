import React from 'react';
import { Form } from 'react-redux-form';
import { FormComponents } from './components/FormComponents';
import { Link, withRouter } from 'react-router-dom';
import { register } from './actions';
import { connect } from 'react-redux';

class ForgotPassword extends React.Component {
    handleChange = e => {
        const { name, value } = e.target;
        const newName = name.slice(name.indexOf('.') + 1);
        this.setState({ [newName]: value });
    };

    handleSubmit() {
        this.props.history.push('/update-password');
    }

    handleSubmitFailed() {}

    render() {
        const { Email, FormButton } = FormComponents;
        return (
            <Form
                className="MyForm"
                model="RestoreUser" //надо подумать к какой модели отнести эту форму
                onSubmit={()=>this.handleSubmit()}
                onSubmitFailed={this.handleSubmitFailed()}
            >
                <h1 className="form_label">Забыли пароль</h1>

                <Email
                    label="Email"
                    id="ForgotPassword.email"
                    handleChange={this.handleChange}
                />

                <div className="form-buttons forgot-button">
                    <FormButton value="Отправить ссылку для восстановления" />
                </div>
                <div className="form-buttons">
                    <Link to="/login" className="link-button">
                        Войти
                    </Link>
                    <Link to="/register" className="link-button">
                        Регистрация
                    </Link>
                </div>
            </Form>
        );
    }
}

function mapState(state) {
    const { isUserLogged, answer } = state.registerReducer;
    return { isUserLogged, answer };
}

const actionCreators = {
    register: register,
};

const connectedForgotPassword = withRouter(
    connect(mapState, actionCreators)(ForgotPassword)
);

export { connectedForgotPassword as ForgotPassword };
