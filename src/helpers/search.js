export  function nameSearch(data, conversations){
    const lowerCaseData = data.toLowerCase();
    
    return conversations.filter(conversations => conversations.authorId.toLowerCase().includes(lowerCaseData))

}
export function messagesSearch(data, messages,conversations ){
    const lowerCaseData = data.toLowerCase();
    const displayedMessages = messages.filter(messages =>
        messages.body.toLowerCase().includes(lowerCaseData));
    const displayedConversationID = displayedMessages.map(displayedMessages =>  displayedMessages.conversationId);
    return conversations.filter(conversations => displayedConversationID.includes(conversations.id));
  }

export function unitedSearch (data, messages,conversations) {
    const name = nameSearch(data, conversations);
    const message = messagesSearch(data, messages,conversations );
    return Array.from(new Set(name.concat(message)));
}