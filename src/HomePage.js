import React from 'react';
import {withRouter} from 'react-router-dom';
import {logout, getProfile, search,getConversations, getMessages} from "./actions";
import {connect} from "react-redux";
import {PageComponents} from './components/PageComponents'
import {userService} from './services/service'
import {toast} from "react-toastify";


class HomePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            search: '',
            id: '',
            messages: [
                {
                    conversationId: ''
                }
            ],
        };

        this.LogOut = this.LogOut.bind(this);
    }
    componentDidMount =() => {
        this.props.getProfile();
        this.props.getConversations();
        this.liveMessages();
        setInterval( this.liveMessages, 30000);
        };

    liveMessages =()=> {
        if(this.props.location.pathname.includes('/conversation/'))
            this.props.getMessages(this.props.location.pathname.slice(14));
    };


    componentDidUpdate(prevProps) {
        if (this.props.location.pathname !== prevProps.location.pathname) {
            this.setState({id: this.props.location.pathname.slice(14)});
            if(this.props.location.pathname.includes('/conversation/'))this.props.getMessages(this.props.location.pathname.slice(14));
        }
    }

    LogOut(){
        console.log(this.props);
        this.props.logout();
        this.props.history.push('/login')
    }



    handleSearchChange = e => {
        const {value} = e.target;
        this.setState({search:value});
        this.props.search(value, this.props.messages, this.props.data);
    };

    saveDialog =(id) => {
        const {changeConversationStatus} = userService;
        const status = 'SAVED';
        changeConversationStatus(id, status).then(data => {
            if(data.status === status)toast.success('Диалог успешно сохранен.')
        })
    };

    beginDialog = (id) => {
        const {changeConversationStatus} = userService;
        const status = 'ACTIVE';
        changeConversationStatus(id, status).then(data => {
            if(data.status === status)toast.success('Вы вошли в дилог.')
        });

        this.props.getMessages(id);
        this.setState({id:id})
};
    deleteDialog = (id) => {
        const {deleteConversation} = userService;
        deleteConversation(id).then(data => {
            toast.success(data.message)
        })

    };

    render() {
        const {SideBar, Header, Body} = PageComponents;
        const {messages, filteredData, currentMessages} = this.props;
        const {id} = this.state;
        //console.log(localStorage.user);
        return (
            <div className="homepage">
                <SideBar/>
                <div className='homepage-container'>
                    <Header
                        email={this.props.email}
                        user={this.props.user}
                        LogOut={this.LogOut}
                        handleSearchChange={this.handleSearchChange}
                    />

                    <Body
                        messages={messages}
                        messagesArray={currentMessages}
                        id={id}
                        dialogs={filteredData}
                        saveDialog={this.saveDialog}
                        beginDialog={this.beginDialog}
                        deleteDialog={this.deleteDialog}
                    />

                </div>
                {/*<button onClick={() => this.showMessages()}>show dialogs</button>*/}
            </div>
        );
    }
}

const mapState = state =>{
    const {user, email} = state.authReducer;
    const {data, messages, filteredData} = state.conversationsReducer;
    const currentMessages = state.messagesReducer.messages;
    return {user, email, data, messages, filteredData,currentMessages}
};

const actionCreators = {
    logout: logout,
    getProfile: getProfile,
    search: search,
    getConversations: getConversations,
    getMessages: getMessages,

};

const connectedHomePage = withRouter(
    connect(mapState,actionCreators )(HomePage)
);

export {connectedHomePage as HomePage}