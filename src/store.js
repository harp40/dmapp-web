import { createStore, applyMiddleware, combineReducers } from 'redux';
import { createForms } from 'react-redux-form';
import {persistStore, persistReducer} from "redux-persist";
import storage from 'redux-persist/lib/storage'
import thunk from 'redux-thunk';
import { messagesReducer,  authReducer, registerReducer, conversationsReducer } from './reducers/reducers';

const initialUserState = {
    email: '',
    password: '',
    confirmPassword: '',
};

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['authReducer'],
};

const combinedReducers = combineReducers({
    authReducer,
    registerReducer,
    conversationsReducer,
    messagesReducer,
    ...createForms({
        LoginUser: initialUserState,
        RegisterUser: initialUserState,
        RestoreUser: initialUserState,
    }),
});

const persistedReducer = persistReducer(persistConfig,combinedReducers);


const store = createStore(persistedReducer,
    applyMiddleware(thunk)
);
export const persistor = persistStore(store);

export default store;
